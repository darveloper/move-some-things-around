//create disk elements = disc
//create rod elements
//show selected disk
//be able to move disks from one rod to another
//make sure bigger disks can't be placed on top of smaller disks



let selectedDisk; //top disk of currently selected rod elements 
let selectedRod; // currently selected rod element
let toggled = false; //alternate toggled state, update on click
let moveCount = 0;
const diskCount = 4
let gameOver = false

// we want to create disk elements
function createDisks() {
    const rodDiv = document.getElementById('rod1')
    for (let i = 1; i <= diskCount; i++) {
        const disk = document.createElement('div')
        disk.classList.add('disk')
        disk.id = 'disk'+i
        disk.style.width = ((diskCount+2)-i)*40+'px'
        rodDiv.appendChild(disk)
    }
}

// we want to create rod/stack elements
function createRods() {
    const main = document.querySelector('main')
    for (let i = 1; i <= 3; i++) {
        const rod = document.createElement('div')
        rod.classList.add('rod')
        rod.id = 'rod'+i
        rod.addEventListener('click', updateDisk)
        main.appendChild(rod)
    }
}

// we want to show that we selected the top disk
function selectDisk() {
    if (selectedRod.childElementCount <= 4) {
        selectedDisk = selectedRod.lastElementChild
        selectedDisk.classList.add('selectedDisk')
    } else {
        toggled = !toggled
    }
}

// we want to move the disk

function moveDisk() {
    if (selectedDisk) {
        if (selectedRod.lastElementChild) {
            if (compareSize(selectedDisk, selectedRod.lastElementChild)) {
                selectedRod.appendChild(selectedDisk)
                selectedDisk.style.border = "none"
            } else {
                selectedDisk.style.border = "none"
                selectedDisk = undefined
            }
        } else {
            selectedRod.appendChild(selectedDisk)
            selectedDisk.style.border = "none"
        }
    }
}

// we want to compare the sizes of the disks
function compareSize (a,b) {
    
    let widthA = window.getComputedStyle(a).getPropertyValue('width')
    let widthB = window.getComputedStyle(b).getPropertyValue('width')
    widthA = parseInt(widthA)
    widthB = parseInt(widthB)
    
    if (widthA < widthB) {
        return true
    } else {
        return false
    }
}

// click handler used to update dom
function updateDisk(event) {
    if(gameOver===false){
        selectedRod = event.currentTarget
    
        if(!toggled) {
            selectDisk()
        } else {
            moveDisk()
        }
    
        toggled = !toggled
        
        checkWin()
    }
}

// check for win condition and display win
function checkWin() {
    if(document.getElementById("rod3").childElementCount == diskCount) {
        alert("You Win!")
        gameOver = true

    }
}









// call init functions to create page
createRods()
createDisks()

